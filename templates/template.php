<html>
<head>
	<meta charset="UTF-8">
	<title>memeMaker</title>
	<link rel="stylesheet" href="https://bootswatch.com/4/sketchy/bootstrap.css">
	<link rel="stylesheet" href="../assets/styles/style.css">
	<link href="https://fonts.googleapis.com/css?family=Anton&display=swap" rel="stylesheet">
</head>
<body>
	<?php require "navbar.php"; ?>
	<?php get_content(); ?>
	<?php require "footer.php";?>
</body>
</html>
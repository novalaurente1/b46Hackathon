<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">memeMaker</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarColor02">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="../views/home.php">Home</a>
      </li>
       <?php
        session_start(); 
        if(isset($_SESSION['users']))
        {
        ?>
      <li class="nav-item">
        <a class="nav-link" href="../views/create_meme.php">Create Meme</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../controllers/logout.php" onclick="window.alert('You have Logged out.');">Log Out</a>
      </li>
       <?php
        }else{
      ?>
      <li class="nav-item">
        <a class="nav-link" href="../views/login.php">Login</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../views/register.php">Register</a>
      </li>
      <?php
        };
      ?>
    </ul>
  </div>
</nav>

<!-- JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
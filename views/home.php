<?php

	require "../templates/template.php"; 
	
?>

<?php function get_content() { 
	
	require "../controllers/connection.php";?>
	<div class="container">
		<div class="row">
			<!-- list -->
			<div class="col-lg-10 offset-lg-1 d-flex flex-wrap">
				
				<?php 
					$images = file_get_contents("../assets/images.json");

					$images_array = json_decode($images, true);
				
					$sql = "SELECT * FROM meme_feed mf 
							LEFT JOIN memes m 
							ON m.id = mf.user_memes_id
							LEFT JOIN users u 
							ON u.id = m.user_id
							
							";

					$query = mysqli_query($conn, $sql);
					
					foreach($query as $key => $value){
						
					?>

						<div class="col-lg-4 py-2">
								<div class="card">
									<div style="border: 3px solid #f2f2f2; display: flex; background-repeat: round; flex-direction: column; justify-content: space-between; margin: 0 auto; width: 250px; height: 250px; background-image: url('../<?php echo $images_array[$value['image']]['image']  ?>') " id="canvas">
                    <div id="topTextDiv" style="width: 100%; height: 50px;"><?php echo $value['toptext'] ?></div>
                    <div id="bottomTextDiv" style="width: 100%; height: 50px;"><?php echo $value['bottomtext'] ?></div>
                </div>
                					<div class="card-body">
                						<h4 class="card-title"><?php echo $value['name'] ?></h4>
                					</div>
								</div>
							</div>
				<?php
					}
				
				?>
			</div>
		</div>
	</div>
<?php } ?>
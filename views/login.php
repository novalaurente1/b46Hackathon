<?php 
    require "../templates/template.php";
    function get_content(){
        ?>
        <h1 class="text-center py-5">Login</h1>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <form action="" method="POST">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" id="email" name="email" class="form-control">
                            <span></span>
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" id="password" name="password" class="form-control">
                    </form>
                        <button class="btn btn-success" id="login-user" type="button">Login</button>
                        </div>
                        <p>
                        Not yet a member?
                            <a href="register.php">Register Now!</a> 
                        </p>
                    </form>
                </div>
            </div>
        </div>
        <script src="../assets/scripts/login.js" type="text/javascript"></script>
        <?php
    }
?>
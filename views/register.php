<?php 
 require "../templates/template.php";
 function get_content(){
 	//require "connection.php";
 	?>
 	<h1 class="text-center py-5">CREATE ACCOUNT</h1>
 	<div class="col-lg-8 offset-lg-2">
            <form action="" method="POST">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" name="email" class="form-control" id="email" required>
                    <span class="validation"></span>
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" class="form-control" id="password" required>
                    <span class="validation"></span>
                </div>
                <div class="form-group">
                    <label for="confirm">Confirm Password</label>
                    <input type="password" name="confirm" class="form-control" id="confirm" required>
                    <span class="validation"></span>
                </div>
            </form>
            <button type="button" class="btn btn-success" id="registerUser">Sign Up!</button>
            <p>Already Registered? <a href="login.php">Login</a></p>
        </div>
        <script src="../assets/scripts/register.js"></script>
<?php
 } //this is the end of the registration page

 ?>
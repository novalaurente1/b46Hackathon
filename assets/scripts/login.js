let loginBtn = document.querySelector("#login-user");

loginBtn.addEventListener('click', () => {
    let email = document.querySelector("#email").value;
    let password = document.querySelector("#password").value;

    let data = new FormData;

    data.append('email', email);
    data.append('password', password);

    fetch("../../controllers/authentication.php", {
        method: 'POST',
        body: data
    })
    .then((response) => {
        return response.text();
    })
    .then((data_from_fetch) => {
        if(data_from_fetch == 'Login Failed!'){
            document.querySelector('#email').nextElementSibling.innerHTML = 'You entered the wrong email or password';

        }else{
		window.alert("Logged in. You may now start Creating your Meme.");
            window.location.replace("../../views/home.php");

        }
    });
});
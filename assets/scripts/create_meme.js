const toptext = document.getElementById('toptext');
const bottomtext = document.getElementById('bottomtext');
const selectedImage = document.getElementById('selected_image');
 
const topTextDiv = document.getElementById('topTextDiv');
const bottomTextDiv = document.getElementById('bottomTextDiv');
 
const canvas = document.getElementById('canvas');
 
toptext.addEventListener('change', (e)=> {
    topTextDiv.innerHTML = e.currentTarget.value;
    
})
 
bottomtext.addEventListener('change', (e)=> {
    bottomTextDiv.innerHTML = e.currentTarget.value;
    
})
 
function selectImage(elem) {
    canvas.style.backgroundImage = "url('" + elem.src + "')";
    selectedImage.value = elem.getAttribute('data-id');
}

const reset = document.getElementById('reset');

reset.addEventListener('click', (e)=> {
	canvas.style.backgroundImage = "";
	topTextDiv.innerHTML = "";
	bottomTextDiv.innerHTML = "";
})

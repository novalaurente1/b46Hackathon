function validate_reg_form(){
	errors = 0;

	let email = document.querySelector("#email").value;
	let password = document.querySelector("#password").value;
	let confirm = document.querySelector("#confirm").value;

	// No empty field
	// username must be at least 8 characters long, max 24
	// password must be at least 8 characters long
	// confirm = password


	if(email == ""){
		document.querySelector("#email").nextElementSibling.innerHTML= "Please provide a valid Email";
		errors++;
	}else{
		document.querySelector("#email").nextElementSibling.innerHTML= "";
	};

	if(password == ""){
		document.querySelector("#password").nextElementSibling.innerHTML= "Please provide a valid First Name";
		errors++;
	}else{
		document.querySelector("#password").nextElementSibling.innerHTML= "";
	};

	if(confirm == ""){
		document.querySelector("#confirm").nextElementSibling.innerHTML= "Please confirm your password";
		errors++;
	}else{
		document.querySelector("#confirm").nextElementSibling.innerHTML= "";
	};

	if(password.length<8){
		document.querySelector("#password").nextElementSibling.innerHTML= "Password should be minimum of eight characters.";
		errors++
	}else{
		document.querySelector("#password").nextElementSibling.innerHTML= "";
	}

	if(password!=confirm){
		document.querySelector("#confirm").nextElementSibling.innerHTML= "Your Password didn't match. Please Try Again";
		errors++;
	}else{
		document.querySelector("#confirm").nextElementSibling.innerHTML= "";
	}

	if (errors> 0){
		return false
	}else{
		return true;
	}
	
}

document.querySelector('#registerUser').addEventListener("click", ()=> {
		if(validate_reg_form()){

		let email = document.querySelector("#email").value;
		let password = document.querySelector("#password").value;
	
		let data = new FormData;
	
		data.append("email", email);
		data.append("password", password);
	
		fetch("../../controllers/process_register.php", {
			method: "POST",
			body: data
		}).then(response=>{
			return response.text();
		}).then(data_from_fetch=>{
			console.log(data_from_fetch);
			if(data_from_fetch=="user_exists"){
				document.querySelector("#email").nextElementSibling.innerHTML = "An account already exist";
			} else{
				window.alert("Congratulations! Your memeMaker account has been created. Please Login to start making Memes.")
			}
		})
	}
})